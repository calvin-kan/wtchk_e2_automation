import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Generate random number'
String memberFirstNameEnglish = 'AutoTesting'

String memberLastNameEnglish = 'ENGeShopper'

String memberMobile = String.valueOf(Math.abs(new Random().nextInt() % (99999999 - 11111111)) + 11111111)

String addressFiller = '43120'

'check elements visible'
WebUI.verifyElementVisible(findTestObject('Checkout/checkout_deliveryInfoSection'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('Checkout/checkout_deliveryOptionsSection'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('Checkout/checkout_codeVoucherSection'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('Checkout/Payment Method/checkout_paymentMethodSection'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('Checkout/checkout_PayBtn'), FailureHandling.CONTINUE_ON_FAILURE)

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Checkout/HD - New Address/checkout_newAddress_firstName'), memberFirstNameEnglish)

WebUI.setText(findTestObject('Checkout/HD - New Address/checkout_newAddress_lastName'), memberLastNameEnglish)

'Enter TH mobile'
WebUI.setText(findTestObject('Checkout/HD - New Address/checkout_newAddress_mobile'), memberMobile)

WebUI.enhancedClick(findTestObject('Checkout/HD - New Address/checkout_newAddress_AreaDropdownBtn'))

WebUI.enhancedClick(findTestObject('Checkout/HD - New Address/checkout_newAddress_Area1'))

WebUI.enhancedClick(findTestObject('Checkout/HD - New Address/checkout_newAddress_DistrictDropdownBtn'))

WebUI.enhancedClick(findTestObject('Checkout/HD - New Address/checkout_newAddress_District1'))

WebUI.setText(findTestObject('Checkout/HD - New Address/checkout_newAddress_Address1'), addressFiller)

WebUI.scrollToElement(findTestObject('Checkout/HD - New Address/checkout_newAddress_AddToAddressBkChkbox'), 0)

WebUI.enhancedClick(findTestObject('Checkout/HD - New Address/checkout_newAddress_AddToAddressBkChkbox'))

WebUI.enhancedClick(findTestObject('Checkout/HD - New Address/checkout_newAddress_Save and Ship to this address'))

'verify address has been added'
WebUI.verifyTextPresent(memberFirstNameEnglish, false)

WebUI.verifyTextPresent(memberMobile, false)

WebUI.verifyTextPresent(addressFiller, false)

