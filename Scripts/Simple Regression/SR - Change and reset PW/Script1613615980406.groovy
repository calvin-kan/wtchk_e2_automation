import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.concurrent.ThreadLocalRandom as Keyword
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

KeywordLogger logger = new KeywordLogger()

'Generate random number'
String randomNumber = String.valueOf(Math.abs(new Random().nextInt() % (99999999 - 11111111)) + 11111111)

'Declare strings'
String memberEmail = (('autotesting-' + randomNumber) + GlobalVariable.BU) + '@yopmail.com'

String memberName = ('Auto Testing ' + randomNumber) + ' member'

String memberMobile = '09' + randomNumber

String memberAddressName = ('Auto Testing' + randomNumber) + 'address 1'

WebUI.openBrowser('')

'Go to WTCHK UAT'
WebUI.navigateToUrl(GlobalVariable.defaultURL)

'Go to my account'
WebUI.enhancedClick(findTestObject('Home/home_MyAccountBtn'))

//Registration
'enhancedClick register button'
WebUI.enhancedClick(findTestObject('Login/login_RegisterButton'))

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_NameField'), memberName)

'Paste email'
WebUI.sendKeys(findTestObject('Registration/registraion_EmailField'), memberEmail)

'Paste email again'
WebUI.sendKeys(findTestObject('Registration/registration_EmailVerificationField'), memberEmail)

'Enter TW mobile'
WebUI.sendKeys(findTestObject('Registration/registration_MobileField'), memberMobile)

'Enter pw'
WebUI.sendKeys(findTestObject('Registration/registration_PasswordField'), GlobalVariable.defaultPassword)

'Enter pw again'
WebUI.sendKeys(findTestObject('Registration/registration_PwVerificationField'), GlobalVariable.defaultPassword)

'submit'
WebUI.enhancedClick(findTestObject('Registration/registration_SubmitButton'))

'enter correct otp'
WebUI.setText(findTestObject('OTP/otp_otpField'), GlobalVariable.defaultOTP)

'submit otp'
WebUI.enhancedClick(findTestObject('OTP/otp_confirmOTPButton'))

'check registration successful'
WebUI.verifyTextPresent(GlobalVariable.reg_Success_Welcome, false)

WebUI.verifyTextPresent(GlobalVariable.reg_Success_MemberText, false)

WebUI.enhancedClick(findTestObject('Home/home_MyAccountBtn'))

'change pw'
WebUI.enhancedClick(findTestObject('AccountSummary/account_changePw'))

WebUI.setText(findTestObject('ChangePassword/changePw_oldPw'), GlobalVariable.defaultPassword)

WebUI.setText(findTestObject('ChangePassword/changePw_newPw'), GlobalVariable.changePassword)

WebUI.setText(findTestObject('ChangePassword/changePw_confirmNewPw'), GlobalVariable.changePassword)

WebUI.enhancedClick(findTestObject('ChangePassword/changePw_updatePwBtn'))

WebUI.verifyElementVisible(findTestObject('ChangePassword/changePw_PwUpdatedMsg'))

'mouse over the my account btn'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

'click the logout btn'
WebUI.enhancedClick(findTestObject('Home/home_logoutBtn'))

WebUI.enhancedClick(findTestObject('Home/home_MyAccountBtn'))

WebUI.setText(findTestObject('Login/login_usernameField'), memberEmail)

WebUI.setText(findTestObject('Login/login__passwordField'), GlobalVariable.changePassword)

'click login btn'
WebUI.enhancedClick(findTestObject('Login/login_loginButton'))

'mouse over the my account btn'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

'click the logout btn'
WebUI.enhancedClick(findTestObject('Home/home_logoutBtn'))

WebUI.enhancedClick(findTestObject('Home/home_MyAccountBtn'))

'forget pw'
WebUI.enhancedClick(findTestObject('Login/login_forgetPw'))

WebUI.enhancedClick(findTestObject('ForgetPassword/forgetPw_emailResetOption'))

WebUI.enhancedClick(findTestObject('ForgetPassword/forgetPw_nextBtn'))

Boolean whileLoopRanOnce = false

Boolean resetPwEmailReceived = false

while (resetPwEmailReceived == false) {
    if (whileLoopRanOnce == true) {
        WebUI.navigateToUrl('https://infwtctwy6uat.aswatson.net/login/pw/forgetByEmail')
    }
    
    WebUI.setText(findTestObject('ForgetPassword/forgetPw_emailField'), memberEmail)

    WebUI.enhancedClick(findTestObject('ForgetPassword/forgetPw_confirmEmailBtn'))

    WebUI.verifyTextPresent(GlobalVariable.pwResetToEmailMsg, false)

    WebUI.delay(10)

    WebUI.navigateToUrl('http://www.yopmail.com/en/')

    WebUI.setText(findTestObject('YopmailHomePage/yopmail_EmailField'), memberEmail)

    WebUI.enhancedClick(findTestObject('YopmailHomePage/yopmail_CheckInboxBtn'))

    'click the link in the email'
    resetPwEmailReceived = WebUI.verifyElementPresent(findTestObject('YopmailInbox/yopmail_otpEmail'), 3, FailureHandling.OPTIONAL)

    whileLoopRanOnce = true
}

WebUI.enhancedClick(findTestObject('YopmailInbox/yopmail_otpEmail'))

WebUI.switchToWindowIndex(1)

WebUI.setText(findTestObject('CreateNewPassword/newPw_newPw'), GlobalVariable.defaultPassword)

WebUI.setText(findTestObject('CreateNewPassword/newPw_ConfirmPw'), GlobalVariable.defaultPassword)

WebUI.enhancedClick(findTestObject('CreateNewPassword/newPw_Next'))

WebUI.verifyTextPresent(GlobalVariable.changePasswordSucessfulMsg, false)

WebUI.verifyTextPresent(memberEmail, false)

WebUI.enhancedClick(findTestObject('CreateNewPassword/newPw_LoginNowBtn'))

'fill in login'
WebUI.setText(findTestObject('Login/login_usernameField'), memberEmail)

'fill in pw'
WebUI.setText(findTestObject('Login/login__passwordField'), GlobalVariable.defaultPassword)

'click login btn'
WebUI.click(findTestObject('Login/login_loginButton'))

'check if reached ac summary page'
WebUI.verifyElementText(findTestObject('AccountSummary/account_AccountSummaryTitle'), GlobalVariable.acSummary_Heading)

logger.logInfo(memberEmail)

