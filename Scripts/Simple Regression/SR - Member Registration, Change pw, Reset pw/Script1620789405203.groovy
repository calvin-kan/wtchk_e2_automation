import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.concurrent.ThreadLocalRandom as Keyword
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement

'Generate random number'
String randomNumber = String.valueOf(Math.abs(new Random().nextInt() % (99999999 - 11111111)) + 11111111)

'Declare strings'
String memberEmail = (('autotesting-' + randomNumber) + GlobalVariable.BU) + '@yopmail.com'

String memberFirstNameEnglish = 'AutoTesting'

String memberLastNameEnglish = 'member'

String memberMobile = randomNumber

String otpEmail = ''

WebUI.openBrowser('')

'Go to WTCHK UAT'
WebUI.navigateToUrl(GlobalVariable.defaultURL)

WebUI.waitForElementPresent(findTestObject('GlobalObjects/homepage_ads'), 3, FailureHandling.OPTIONAL)

WebUI.enhancedClick(findTestObject('GlobalObjects/homepage_ads'), FailureHandling.OPTIONAL)

WebDriver driver = DriverFactory.getWebDriver()

WebElement watsonLogo = driver.findElement(By.xpath('//cx-page-slot[@class="SiteLogo has-components ng-star-inserted"]/*/*/*/img'))

Actions actions = new Actions(driver)

actions.keyDown(Keys.SHIFT).click(watsonLogo).keyUp(Keys.SHIFT).perform()

WebUI.switchToWindowIndex(0)

'Go to my account'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

WebUI.enhancedClick(findTestObject('Home/home_loginBtn'))

//Registration
'click register button'
WebUI.enhancedClick(findTestObject('Login/login_RegisterButton'))

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_english_firstName'), memberFirstNameEnglish)

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_english_lastName'), memberLastNameEnglish)

'Paste email'
WebUI.sendKeys(findTestObject('Registration/registraion_EmailField'), memberEmail)

'Enter mobile number'
WebUI.setText(findTestObject('OTP/otp_mobileField'), memberMobile)

'Click request OTP button'
WebUI.enhancedClick(findTestObject('OTP/otp_requestOTPBtn'))

'Fill in OTP'
WebUI.setText(findTestObject('OTP/otp_otpField'), GlobalVariable.defaultOTP)

'Enter pw'
WebUI.sendKeys(findTestObject('Registration/registration_PasswordField'), GlobalVariable.defaultPassword)

'submit'
WebUI.enhancedClick(findTestObject('Registration/registration_SubmitButton'))

'submit'
WebUI.enhancedClick(findTestObject('Registration/registration_SubmitButton'))

WebUI.verifyTextPresent(GlobalVariable.reg_Success_Welcome, false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyTextPresent(GlobalVariable.reg_Success_MemberText, false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

WebUI.enhancedClick(findTestObject('Home/home_accountSummaryLink'))

'check if reached ac summary page'
WebUI.verifyElementVisible(findTestObject('AccountSummary/account_AccountSummaryTitle'), FailureHandling.STOP_ON_FAILURE)

KeywordLogger logger = new KeywordLogger()

logger.logInfo(memberEmail)

'change pw'
WebUI.enhancedClick(findTestObject('AccountSummary/account_changePw'))

'enter old pw'
WebUI.setText(findTestObject('ChangePassword/changePw_oldPw'), GlobalVariable.defaultPassword)

'enter new pw'
WebUI.setText(findTestObject('ChangePassword/changePw_newPw'), GlobalVariable.changePassword)

'enter new pw again'
WebUI.setText(findTestObject('ChangePassword/changePw_confirmNewPw'), GlobalVariable.changePassword)

'click update pw'
WebUI.enhancedClick(findTestObject('ChangePassword/changePw_updatePwBtn'))

'mouse over the my account btn'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

'click the logout btn'
WebUI.enhancedClick(findTestObject('Home/home_logoutBtn'))

'Go to my account'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

WebUI.enhancedClick(findTestObject('Home/home_loginBtn'))

'enter login email'
WebUI.setText(findTestObject('Login/login_usernameField'), memberEmail)

'enter pw'
WebUI.setText(findTestObject('Login/login__passwordField'), GlobalVariable.changePassword)

'click login btn'
WebUI.enhancedClick(findTestObject('Login/login_loginButton'))

'mouse over the my account btn'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

'click the logout btn'
WebUI.enhancedClick(findTestObject('Home/home_logoutBtn'))

'Go to my account'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

WebUI.enhancedClick(findTestObject('Home/home_loginBtn'))

'forget pw'
WebUI.enhancedClick(findTestObject('Login/login_forgetPw'))

'click reset pw by email option'
WebUI.check(findTestObject('ForgetPassword/forgetPw_emailResetOption'))

'click next'
WebUI.enhancedClick(findTestObject('ForgetPassword/forgetPw_nextBtn'))

'set loop flag to false'
Boolean whileLoopRanOnce = false

'set email received flag to false'
Boolean resetPwEmailReceived = false

'initialise run limit'
int runLimit = 0

'run loop as long as email is not received but will stop when runLimit reaches 5'
while ((resetPwEmailReceived == false) && (runLimit <= 5)) {
    'run if loop flag is true'
    if (whileLoopRanOnce == true) {
        'go to reset pw page'
        WebUI.navigateToUrl(GlobalVariable.forgetByEmailPageURL)
    }
    
    'enter email '
    WebUI.setText(findTestObject('ForgetPassword/forgetPw_emailField'), memberEmail)

    'confirm email'
    WebUI.enhancedClick(findTestObject('ForgetPassword/forgetPw_confirmEmailBtn'))

    'wait 10 sec'
    WebUI.delay(10)

    WebUI.enhancedClick(findTestObject('ForgetPassword/forgotPw_otpEmail'))

    WebUI.switchToWindowIndex(1)

    'go to yopmail'
    WebUI.navigateToUrl('http://www.yopmail.com/en/')

    'enter email'
    WebUI.setText(findTestObject('YopmailHomePage/yopmail_EmailField'), memberEmail)

    'click check email button'
    WebUI.enhancedClick(findTestObject('YopmailHomePage/yopmail_CheckInboxBtn'))

    WebUI.switchToFrame(findTestObject('YopmailInbox/iframe_Headers_ifmail'), 5)

    resetPwEmailReceived = WebUI.verifyElementPresent(findTestObject('YopmailInbox/yopmail_otpEmail'), 3, FailureHandling.OPTIONAL)

    String[] otpStr = WebUI.getText(findTestObject('YopmailInbox/yopmail_otpEmail')).split(' ')

    otpEmail = (otpStr[4])

    'change loop flag to true'
    whileLoopRanOnce = true

    runLimit++
}

'switch to new tab'
WebUI.switchToWindowIndex(0)

WebUI.setText(findTestObject('OTP/otp_otpEmailField'), otpEmail)

WebUI.enhancedClick(findTestObject('ForgetPassword/forgetPw_byEmailSubmitBtn'))

'enter new pw'
WebUI.setText(findTestObject('CreateNewPassword/newPw_newPw'), GlobalVariable.defaultPassword)

'enter new pw again'
WebUI.setText(findTestObject('CreateNewPassword/newPw_ConfirmPw'), GlobalVariable.defaultPassword)

'click next'
WebUI.enhancedClick(findTestObject('CreateNewPassword/newPw_Next'))

'fill in login'
WebUI.setText(findTestObject('Login/login_usernameField'), memberEmail)

'fill in pw'
WebUI.setText(findTestObject('Login/login__passwordField'), GlobalVariable.defaultPassword)

'click login btn'
WebUI.click(findTestObject('Login/login_loginButton'))

WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

WebUI.enhancedClick(findTestObject('Home/home_accountSummaryLink'))

'check if reached ac summary page'
WebUI.verifyElementText(findTestObject('AccountSummary/account_AccountSummaryTitle'), GlobalVariable.acSummary_Heading)

