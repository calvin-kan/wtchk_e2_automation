import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Generate random number'
String randomNumber = String.valueOf(Math.abs(new Random().nextInt() % 99) + 1)

String randomPhoneNumber = '09' + String.valueOf(Math.abs(new Random().nextInt() % (99999999 - 11111111)) + 11111111)

String randomAddress = String.valueOf(randomNumber)

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.defaultURL)

WebUI.waitForElementPresent(findTestObject('GlobalObjects/homepage_ads'), 3)

WebUI.enhancedClick(findTestObject('GlobalObjects/homepage_ads'))

WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

WebUI.enhancedClick(findTestObject('Home/home_loginBtn'))

'fill in login'
WebUI.setText(findTestObject('Login/login_usernameField'), GlobalVariable.defaultLogin)

'fill in pw'
WebUI.setText(findTestObject('Login/login__passwordField'), GlobalVariable.defaultPassword)

'enhancedClick login btn'
WebUI.enhancedClick(findTestObject('Login/login_loginButton'))

WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

WebUI.enhancedClick(findTestObject('Home/home_accountSummaryLink'))

'check if reached ac summary page'
WebUI.verifyElementText(findTestObject('AccountSummary/account_AccountSummaryTitle'), GlobalVariable.acSummary_Heading)

'enhancedClick update info link'
WebUI.enhancedClick(findTestObject('AccountSummary/account_UpdatePerInfo'), FailureHandling.STOP_ON_FAILURE)

'enhancedClick edit info btn'
WebUI.enhancedClick(findTestObject('AccountSummary/account_EditPerInfoButton'))

'update phone number'
WebUI.setText(findTestObject('AccountSummary/account_MobileField'), randomPhoneNumber)

'fill in the fields in the address with random number'
WebUI.setText(findTestObject('AccountSummary/account_StreetNameField'), randomAddress)

WebUI.setText(findTestObject('AccountSummary/account_SectionField'), randomAddress)

WebUI.setText(findTestObject('AccountSummary/account_AlleyField'), randomAddress)

WebUI.setText(findTestObject('AccountSummary/account_LaneField'), randomAddress)

WebUI.setText(findTestObject('AccountSummary/account_HouseNoField'), randomAddress)

WebUI.setText(findTestObject('AccountSummary/account_FloorField'), randomAddress)

WebUI.setText(findTestObject('AccountSummary/account_RoomField'), randomAddress)

'update the address'
WebUI.enhancedClick(findTestObject('AccountSummary/account_ConfirmUpdatePerInfoBtn'))

'enter otp'
WebUI.setText(findTestObject('AccountSummary/account_updateMobileOTP'), GlobalVariable.defaultOTP)

WebUI.enhancedClick(findTestObject('AccountSummary/account_updateMobileSubmitOTPBtn'))

'check if changes were made correctly'
WebUI.verifyElementText(findTestObject('AccountSummary/account_FullAddressText'), randomAddress+randomAddress+'段'+randomAddress+'巷'+randomAddress+'弄'+randomAddress+'號'+randomAddress+'樓'+randomAddress+'之')

WebUI.verifyTextPresent(randomPhoneNumber, false)

'mouse over the my account btn'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

'enhancedClick the logout btn'
WebUI.enhancedClick(findTestObject('Home/home_logoutBtn'))

