import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.navigateToUrl(GlobalVariable.defaultURL)

//Delete items in shopping cart if found any
Boolean shoppingCartItemCountNotZero = !(WebUI.getText(findTestObject('GlobalObjects/basket_ItemCount')).equals('0'))

'see if there is anything in the cart, delete if any'
if (shoppingCartItemCountNotZero) {
    'go to basket'
    WebUI.enhancedClick(findTestObject('GlobalObjects/basket_Icon'))

    Boolean crossBtnFound = WebUI.verifyElementPresent(findTestObject('Basket/basket_DeleteProductBtn'), 1, FailureHandling.OPTIONAL)

    while (crossBtnFound) {
        WebUI.enhancedClick(findTestObject('Basket/basket_DeleteProductBtn'), FailureHandling.CONTINUE_ON_FAILURE)

        crossBtnFound = WebUI.verifyElementPresent(findTestObject('Basket/basket_DeleteProductBtn'), 1, FailureHandling.OPTIONAL)
    }
}

WebUI.navigateToUrl(GlobalVariable.fd_productUrl)

WebUI.enhancedClick(findTestObject('PDP_BrandProduct1/PDP_BrandProduct1_AddQtyBtn'))

WebUI.enhancedClick(findTestObject('PDP_BrandProduct1/PDP_BrandProduct1_AddToBasketBtn'))

'go to basket'
WebUI.enhancedClick(findTestObject('GlobalObjects/basket_Icon'))

WebUI.focus(findTestObject('Basket/Delivery Method/basket_CCchkbox'))

'select CC'
WebUI.enhancedClick(findTestObject('Basket/Delivery Method/basket_CCchkbox'))

'apply evoucher if enabled in global variable'
if (GlobalVariable.eVoucherTest) {
    WebUI.setText(findTestObject('Basket/basket_promoCodeField'), GlobalVariable.eVoucher)

    WebUI.enhancedClick(findTestObject('Basket/basket_applyPromoBtn'))
}

'checkout'
WebUI.enhancedClick(findTestObject('Basket/basket_leftCheckoutBtn'))

'redeem points if point redemption test is required'
if (GlobalVariable.pointRedemptionTest) {
    WebUI.mouseOver(findTestObject('Checkout/checkout_eWallet'))

    'open ewallet'
    WebUI.enhancedClick(findTestObject('Checkout/checkout_eWallet'))

    WebUI.setText(findTestObject('Checkout/checkout_pointRedemptionField'), GlobalVariable.redeemAmount)

    WebUI.enhancedClick(findTestObject('Checkout/checkout_pointRedemptionbtn'))
}

WebUI.enhancedClick(findTestObject('Checkout/CC - Select Store/checkout_selectCCStoreBtn'))

WebUI.enhancedClick(findTestObject('Checkout/CC - Select Store/checkout_selectCCStore_confirmStoreBtn'))

'select payment method'
WebUI.enhancedClick(findTestObject('Checkout/Payment Method/checkout_CreditCardOneTimePaymentBtn'))

WebUI.enhancedClick(findTestObject('Checkout/Payment Method/checkout_aeRadioBtn'))

'pay'
WebUI.enhancedClick(findTestObject('Checkout/checkout_PayBtn'))

WebUI.delay(15)

WebUI.enhancedClick(findTestObject('PaymentGateway/AE/ae_AmexOption'))

'enter credit card credentials'
WebUI.setText(findTestObject('PaymentGateway/AE/ae_cardNo'), GlobalVariable.aeCardNo)

WebUI.setText(findTestObject('PaymentGateway/AE/ae_expMonth'), GlobalVariable.aeCardExpMonth, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('PaymentGateway/AE/ae_expYear'), GlobalVariable.aeCardExpYear, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('PaymentGateway/AE/ae_cvv'), GlobalVariable.aeCardCvv)

'confirm credentials'
WebUI.enhancedClick(findTestObject('PaymentGateway/AE/ae_payBtn'))

WebUI.enhancedClick(findTestObject('PaymentGateway/Emulator/emulatorSubmitBtn'))

WebUI.delay(15)

'check thank you page heading visible'
WebUI.verifyTextPresent(GlobalVariable.thankYouPageHeading, false)

GlobalVariable.pt_cce_ae = WebUI.getText(findTestObject('ThankYouPage/thankYouPage_orderNumber'))

