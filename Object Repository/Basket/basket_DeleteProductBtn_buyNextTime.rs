<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>basket_DeleteProductBtn_buyNextTime</name>
   <tag></tag>
   <elementGuidId>405687bf-fe6f-4a47-8e8e-c7fffb69d676</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//cx-page-slot[contains(@class,'ShopBagNextTimePurchase')]/*/*/*/div[contains(@class,'item ')]/div[contains(@class,'product-detail')]/div[contains(@class,'product-remove')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
