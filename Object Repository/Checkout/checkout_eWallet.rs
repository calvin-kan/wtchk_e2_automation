<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>checkout_eWallet</name>
   <tag></tag>
   <elementGuidId>03d7b139-54de-43a0-ba07-4e55d3d0e320</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.header</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class,'section-title') and contains(text(),'使用折扣碼或折價券')]/..</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>header</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            






    
    
        
    






使用折價券/提貨券或其他抵用方式：

    有寵i卡點數、折價序號、折價券、提貨券、我的儲值卡或禮物卡？
    
    
        &lt;注意~若遇網路流量壅塞，使用提貨劵將可能會有異常情況發生，請稍後再試即可，謝謝>
    

已使用
未使用

        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainContent&quot;)/main[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;container-left&quot;]/section[@class=&quot;container-checkout-codeVoucher&quot;]/div[@class=&quot;checkout-codeVoucher expandable&quot;]/div[@class=&quot;header&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='mainContent']/main/div/div/section[3]/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='更改取貨地點'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='基隆市七堵區工建路1-15號壹樓'])[1]/following::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section[3]/div/div</value>
   </webElementXpaths>
</WebElementEntity>
