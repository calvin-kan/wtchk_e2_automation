<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>checkout_newAddress_DistrictDropdownBtn</name>
   <tag></tag>
   <elementGuidId>31fbc1d9-82c0-4c02-9bad-318809b4b3c4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='createHomeDeliveryInfo__wtchk_createHomeDeliveryInfo_Step_1__9__district']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
