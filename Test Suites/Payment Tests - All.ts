<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Payment Tests - All</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>3b66c612-b41a-4a36-a32b-f4b705cb10c0</testSuiteGuid>
   <testCaseLink>
      <guid>f9af190d-98ee-457b-89d9-ed2ba82642f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/PT - Member Login</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d62ffa29-a6b7-4fee-924f-072860b00619</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>79cd5624-bce5-4f1c-a970-4507589cf37d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9e5838df-d9e8-44f4-9dc0-abd7fbbbd120</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/HD/PT - HD - Visa or Master</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>acd6503e-ef05-4c69-a59c-55e5aca9b65b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/HD/PT - HD - AE</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c7d353a-df19-4aab-8eec-182566c00f5f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/HD/PT - HD - Union Pay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7148c9e4-779c-490b-bed9-ee8a86e121b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/FD/PT - FD - Visa or Master</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49365577-bc77-44b2-99b9-a07e10337d2f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/FD/PT - FD - AE</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33d78854-5f1d-4035-a9b5-5ef2e84d1ca2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/FD/PT - FD - Union Pay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b6e748fa-b71c-4b05-979a-cd46318a1956</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCS/PT - CCS - Visa or Master</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>560bfe68-dae5-4d0d-8788-b5c331bfc6c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCS/PT - CCS - AE</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a11ebf18-90aa-4eee-b7d7-de9250ef5b39</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCS/PT - CCS - Union Pay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eac7889d-58c8-4d85-a936-ec4ab876fb25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCE/PT - CCE - Visa or Master</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df4f99b1-ab1a-4c56-9ce3-d8a6a913a0d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCE/PT - CCE - AE</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ef9f254c-5d1a-4f62-b299-8a248f7d53b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CCE/PT - CCE - Union Pay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bcfc662f-50c4-4b10-88d0-6605d7369d9f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/PT - Display Order Numbers</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
